extern crate argparse;
extern crate serial;

use std::io::prelude::*;
use std::net::UdpSocket;
use std::process;
use std::time::Duration;

use argparse::{ArgumentParser, Store};
use serial::BaudRate;
use serial::prelude::*;

enum ExitCode {
    ArgumentError = 1,
    BadPort,
    ConfigurationError
}

fn main() {
    // Store command-line arguments
    let mut serial_port_name = "".to_string();
    let mut serial_baud = "115200".to_string();
    let mut udp_rx_socket = "".to_string();
    let mut udp_tx_socket = "".to_string();

    // Parse command-line arguments
    {
        let mut ap = ArgumentParser::new();
        ap.set_description("A UDP to serial bridge.");
        ap.refer(&mut serial_port_name)
            .add_option(&["-p", "--port"], Store, "The serial port name (COM3, /dev/ttyUSB0, etc.)")
            .required();
        ap.refer(&mut serial_baud)
            .add_option(&["-b", "--baud"], Store, "The serial port baud rate (default 115200)");
        ap.refer(&mut udp_rx_socket)
            .add_option(&["-r", "--rx-socket"], Store, "The UDP socket to listen on.")
            .required();
        ap.refer(&mut udp_tx_socket)
            .add_option(&["-t", "--tx-socket"], Store, "The UDP socket to transmit on.")
            .required();
        ap.parse_args_or_exit();
    }

    // Convert arguments to numbers
    let baud : usize = match serial_baud.parse() {
        Ok(t) => t,
        Err(_) => { println!("Improper serial baud rate specified ({})", serial_baud); process::exit(ExitCode::ArgumentError as i32)}
    };
    let rx_socket_num: usize = match udp_rx_socket.parse() {
        Ok(t) => t,
        Err(_) => { println!("Improper UDP reception socket specified ({})", udp_rx_socket); process::exit(ExitCode::ArgumentError as i32)}
    };
    let tx_socket_num: usize = match udp_tx_socket.parse() {
        Ok(t) => t,
        Err(_) => { println!("Improper UDP transmission socket specified ({})", udp_tx_socket); process::exit(ExitCode::ArgumentError as i32)}
    };
    let udp_max_size: usize = 255;
    let serial_process_size: usize = 255;

    // Open the specified serial port
    let mut port = match serial::open(&serial_port_name) {
        Ok(m) => { m }
        Err(e) => { println!("Failed to open {}: {}", serial_port_name, e.to_string()); process::exit(ExitCode::BadPort as i32)}
    };

    // Configure the port settings
    match port.reconfigure(&|settings| {
        try!(settings.set_baud_rate(BaudRate::from_speed(baud)));
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    }) {
        Ok(_) => (),
        Err(e) => { println!("Failed to configure {}: {}", serial_port_name, e.to_string()); process::exit(ExitCode::ConfigurationError as i32)}
    }

    // Open the UDP receive port
    let rx_socket = match UdpSocket::bind(("localhost", rx_socket_num as u16)) {
        Ok(t) => t,
        Err(_) => { println!("Unable to bind to localhost:{}. Check that port isn't already in use.", rx_socket_num); process::exit(ExitCode::BadPort as i32)}
    };
    match rx_socket.set_read_timeout(Some(Duration::from_millis(1))) {
        Ok(_) => (),
        Err(e) => {println!("set_read_timeout error ({})", e); process::exit(ExitCode::ConfigurationError as i32)}
    }

    // Open the UDP transmission port
    let tx_socket = match UdpSocket::bind(("localhost", tx_socket_num as u16)) {
        Ok(t) => t,
        Err(_) => { println!("Unable to bind to localhost:{}. Check that port isn't already in use.", tx_socket_num); process::exit(ExitCode::BadPort as i32)}
    };

    // Start the main event loop. This loop is driven by receiving data over the UDP socket.
    let mut udp_buf: Vec<u8> = vec![0; udp_max_size];
    loop {
        let rx_data_len = match rx_socket.recv_from(udp_buf.as_mut_slice()) {
            Ok(t) => { println!("{} received from {}", t.0, t.1); t.0},
            Err(_) => 0
        };

        if rx_data_len > 0 {
            match port.write(&udp_buf[..rx_data_len]) {
                Ok(_) => (),
                Err(e) => println!("ERROR: Failed to write all bytes to serial port ({})", e)
            }
        }
    }
}
